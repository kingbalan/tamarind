var express = require('express');
var router = express.Router();
var request = require('request');
var sizeOf = require('image-size');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Tamarind Bonsai Kottayam - Browse and buy Bonsai plants , tools and resources in Kottayam , Kerala' });
});


router.get('/explore', function(req, res, next) {

	   getPhotosFromFlickr(function(data){
	   		console.log(data)
	   		res.render('gallery', { files:data,title: 'Gallery | Tamarind Bonsai Kottayam - Browse and buy Bonsai plants , tools and resources in Kottayam , Kerala' });
	   })
 
});

function getPhotosFromFlickr(cb){
	var endpoint = "https://api.flickr.com/services/rest/?method=flickr.people.getPublicPhotos&user_id=148788824@N07&format=json&nojsoncallback=1&api_key=16017d78d72e5a2f53e2ec38ed180f06&gallery_id=72157678182521264&extras=url_s,url_t,url_l,description"
	request.get(endpoint,function(a,b,c){
	
		cb(JSON.parse(c).photos.photo)
	})

}

module.exports = router;


